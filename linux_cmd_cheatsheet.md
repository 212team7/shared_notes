
# Linux Terminal Cheat Sheet

Command   |Arguments              |Description
----------|-----------------------|---
ssh pi@<ip_address>|-A(with ssh-agent)|secure shell
cd | .. (back) ~ (home)|Change Directory
ls | -l -a (show hidden)|list files
nano| 'filename'|text editor
rm|'filename'|removes files
rm|-rf|remove dir without prompts
mkdir|DIR|create folder
mv |FILENAME DIR|move 
mv |FILENAME FILENAME|change name
touch |FILE|create file
cp |FILE DESTINATION|copy file
nmtui||network manager gui in CLI
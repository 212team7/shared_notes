## Commands
Descriptions|Command linux |Command Windows
----------|-----------------------|---
pings address "count" times|ping xx.xx.xx.xx -c count|ping xx.xx.xx.xx -n count
Removes all entries in the ARP table|sudo ip -s -s neigh flush all| 
routing table|route -n|
routing table|ip route|
shows arb table|ip neigh|
list MAC table in list|sudo brctl showmacs br0
show mac and interface|bridge fdb show \| grep 'vlan'|
## SRX Router
Ctrl+Alt to unfocus peripherials from vmware
User: root
Password: Rootpass
Descriptions|Command
----------|-----------------------
Enter the command line interface|cli
Enter configuration mode|edit
Set password|edit system root-authentication<br />set plain-text-password
Set router name|set system host-name <name>
Show routing table|run show route terse
Show interface table|interfaces terse
Load plain config file|load override terminal
Commit configuration changes|commit
## Spanning Tree Protocol
STP traffic is sent using the MAC addresses of the switches.
Turn on STP, br0| sudo brctl stp br0 on

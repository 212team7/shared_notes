# Raspberry Pi cheatsheet
## Forwarding SSH key with SSH agent
### In bash

cat ~/.ssh/id_ed25519.pub | ssh pi@raspberrypi "cat >> ~/.ssh/authorized_keys"  

eval $(ssh-agent -s)  

ssh-add ~/.ssh/id_ed25519  

ssh -A pi@raspberrypi  

### On RasPi

ssh -T git@gitlab.com  
## Use ssh key for log in
ssh-copy-id \<user>@\<host>
## Internet via ethernet from Windows.
### On Windows
Go to 'View network connections" &rarr;
Right click Wifi &rarr;
Go to properties &rarr;
Sharing &rarr;
check "Allow other users to connect through this computer's internet connections" &rarr;
In the dropdown menu below select ethernet. \
Open CMD &rarr;
'ipconfig' &rarr;
Under 'Ethernet adapter Ethernet:' note the IPv4 and Subnet Mask.
### On Raspberry Pi
REMOVE DHCP  
sudo apt purge openresolv dhcpcd5

In network manager set up a new network\
Set interface to eth0  
Set the address to the same as the IPv4 address but change the last number.  
Set the netmask to the same as the Subnet Mask from windows.  
Set the gateway to the exact same as the IPv4 from windows.  
Set the DNS to the exact same as the IPv4 from windows.
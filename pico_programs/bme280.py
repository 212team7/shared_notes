from machine import Pin, SoftI2C # from the machine library
import BME280 as bme280 # the bme280 library
from time import sleep
i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)

bme = bme280.BME280(i2c=i2c, address=0x77)


while True:
    for obj in bme.values:
        print(obj)
    print(5*"-")
    sleep(5)

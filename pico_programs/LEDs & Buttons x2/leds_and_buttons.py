from machine import Pin
from utime import sleep

led1 = Pin(16, Pin.OUT)
led2 = Pin(17, Pin.OUT)
btn1 = Pin(2, Pin.IN, Pin.PULL_DOWN)
btn2 = Pin(3, Pin.IN, Pin.PULL_DOWN)

while True:
    if(btn1.value()):
        led1.toggle()
        sleep(0.25)
    elif(btn2.value()):
        led2.toggle()
        sleep(0.25)
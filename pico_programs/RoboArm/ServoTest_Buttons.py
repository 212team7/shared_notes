from machine import Pin, PWM, ADC
from time import sleep

servoPin = PWM(Pin(1))
servoPin.freq(50)
button1 = Pin(14)
button2 = Pin(15)
turn = 4800 #start posetion (90 degrees)
speed = 0.05 #turnspeed
yAxis = ADC(Pin(27))
xAxis = ADC(Pin(26))
button = Pin(16,Pin.IN, Pin.PULL_UP)

servoPin.duty_u16(4800) 
while True:
    xValue = xAxis.read_u16()
    yValue = yAxis.read_u16()
    buttonValue = button.value()
    xStatus = "middle"
    yStatus = "middle"
    buttonStatus = "not pressed"
    
    if xValue <= 600:
        xStatus = "left"
    elif xValue >= 60000:
        xStatus = "right"
    if yValue <= 600:
        yStatus = "up"
    elif yValue >= 60000:
        yStatus = "down"
    if buttonValue == 0:
        buttonStatus = "pressed"
    if xValue >= 60000:
        sleep(speed)
        turn += 100 #turnspeed
        servoPin.duty_u16(turn)
        if turn > 8000: turn = 8000
    elif xValue <= 600:
        sleep(speed) #turnspeed
        turn -= 100
        servoPin.duty_u16(turn)
        if turn < 1500: turn = 1500
        
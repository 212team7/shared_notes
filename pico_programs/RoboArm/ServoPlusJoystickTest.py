from machine import Pin, PWM, ADC
import utime

servoPin = PWM(Pin(1))
servoPin.freq(50)
button1 = Pin(14)
button2 = Pin(15)
turn = 4800 #start posetion (90 degrees)
speed = 0.05 #turnspeed
yAxis = ADC(Pin(27))
xAxis = ADC(Pin(26))
button = Pin(16,Pin.IN, Pin.PULL_UP)

pin_a = Pin(10, Pin.OUT)
pin_b = Pin(11, Pin.OUT) 
pin_c = Pin(12, Pin.OUT)
pin_d = Pin(13, Pin.OUT)

# Set pins to low
pin_a.low()
pin_b.low()
pin_c.low()
pin_d.low()


rotate_steps_cw = [[1,0,0,1],[1,1,0,0],[0,1,1,0],[0,0,1,1]]

rotate_steps_ccw = [[0,0,1,1],[0,1,1,0],[1,1,0,0],[1,0,0,1]]

number_of_steps = 1
max_steps = 100

button_cw = Pin(14, Pin.IN, Pin.PULL_DOWN)
button_ccw = Pin(15, Pin.IN, Pin.PULL_DOWN)

rotation = "cw"

servoPin.duty_u16(4800) 
while True:
    xValue = xAxis.read_u16()
    yValue = yAxis.read_u16()
    buttonValue = button.value()
    xStatus = "middle"
    yStatus = "middle"
    buttonStatus = "not pressed"
    
    if xValue <= 600:
        xStatus = "left"
    elif xValue >= 60000:
        xStatus = "right"
    if yValue <= 600:
        yStatus = "up"
    elif yValue >= 60000:
        yStatus = "down"
    if buttonValue == 0:
        buttonStatus = "pressed"
    if yValue >= 60000:
        utime.sleep(speed)
        turn += 100 #turnspeed
        servoPin.duty_u16(turn)
        if turn > 8000: turn = 8000
    elif yValue <= 600:
        utime.sleep(speed) #turnspeed
        turn -= 100
        servoPin.duty_u16(turn)
        if turn < 1500: turn = 1500
    
    rotation = "stop"
    
    # Read cw button
    if xValue <= 600:
        rotation = "cw"
        
    # Read cw button
    if xValue >= 60000:
        rotation = "ccw"
    
    # Rotation stepper motor in clockwise direction
    if rotation == "cw":
        for step in rotate_steps_cw:
            pin_a.value(step[0])
            pin_b.value(step[1])
            pin_c.value(step[2])
            pin_d.value(step[3])
            utime.sleep(0.002)

    # Rotation stepper motor in counterclockwise direction
    if rotation == "ccw":
        for step in rotate_steps_ccw:
            pin_a.value(step[0])
            pin_b.value(step[1])
            pin_c.value(step[2])
            pin_d.value(step[3])
            utime.sleep(0.002)
     
    number_of_steps += 1
    if number_of_steps == max_steps:
        pin_a.low()
        pin_b.low()
        pin_c.low()
        pin_d.low()
        rotation = "stop"
        
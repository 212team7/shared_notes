from machine import Pin
import utime

# Define output pins
pin_a = Pin(10, Pin.OUT)
pin_b = Pin(11, Pin.OUT) 
pin_c = Pin(12, Pin.OUT)
pin_d = Pin(13, Pin.OUT)

# Set pins to low
pin_a.low()
pin_b.low()
pin_c.low()
pin_d.low()

# Rotate in clockwise direction, adding step to list
#step     1  2  3  4
#        ------------
#pin_a    1  1  0  0
#pin_b    0  1  1  0
#Pin_c    0  0  1  1
#Pin_d    1  0  0  1
#         |  |  |  \_________________________________
#         |  |  \__________________________          |
#         |  \___________________          |         |
#         \____________          |         |         |
#                      |         |         |         |
#                      v         v         v         v
rotate_steps_cw = [[1,0,0,1],[1,1,0,0],[0,1,1,0],[0,0,1,1]]

# Rotate counterclockwise direction
# step    1  2  3  4
#         ----------
# pin_a   0  0  1  1
# pin_b   0  1  0  0
# pin_c   1  1  0  0
# pin_d   1  0  0  1

rotate_steps_ccw = [[0,0,1,1],[0,1,1,0],[1,1,0,0],[1,0,0,1]]

# Define number of stepper moter steps and max steps when to stop
number_of_steps = 1
max_steps = 100

# Define buttons for direction of rotate
button_cw = Pin(14, Pin.IN, Pin.PULL_DOWN)
button_ccw = Pin(15, Pin.IN, Pin.PULL_DOWN)

# Define direction of rotation on start
rotation = "cw"
turns = 1

while True:
    rotation = "stop"
    
    # Read cw button
    if button_cw.value() ==1:
        rotation = "cw"
        
    # Read cw button
    if button_ccw.value() ==1:
        rotation = "ccw"
    
    # Rotation stepper motor in clockwise direction
    if rotation == "cw":
        for step in rotate_steps_cw:
            pin_a.value(step[0])
            pin_b.value(step[1])
            pin_c.value(step[2])
            pin_d.value(step[3])
            utime.sleep(0.002)

    # Rotation stepper motor in counterclockwise direction
    if rotation == "ccw":
        for step in rotate_steps_ccw:
            pin_a.value(step[0])
            pin_b.value(step[1])
            pin_c.value(step[2])
            pin_d.value(step[3])
            utime.sleep(0.002)
     
    number_of_steps += 1
    if number_of_steps == max_steps:
        pin_a.low()
        pin_b.low()
        pin_c.low()
        pin_d.low()
        rotation = "stop"
        
   
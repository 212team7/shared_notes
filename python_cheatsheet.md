# Python Cheatsheet
## Virtual environment
From bash
```bash
py -m venv 'name'
source env/Scripts/activate
deactivate
```